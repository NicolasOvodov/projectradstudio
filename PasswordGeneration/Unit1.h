//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *Button1;
	TLabel *Label1;
	TLayout *Layout1;
	TButton *Button2;
	TEdit *Ed1;
	TCheckBox *CB1;
	TCheckBox *CB2;
	TCheckBox *CB3;
	TCheckBox *CB4;
	TLabel *Label2;
	TNumberBox *NB1;
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm1 *Fm1;
//---------------------------------------------------------------------------
#endif
