//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TPDD *PDD;
//---------------------------------------------------------------------------
__fastcall TPDD::TPDD(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TPDD::BuTheoryClick(TObject *Sender)
{
	 tc->ActiveTab = Theory1;
}
//---------------------------------------------------------------------------
void __fastcall TPDD::BuTestClick(TObject *Sender)
{
	 tc->ActiveTab = TestPage1;
}
//---------------------------------------------------------------------------
void __fastcall TPDD::BuAboutClick(TObject *Sender)
{
	 tc->ActiveTab = About;
}
//---------------------------------------------------------------------------
void __fastcall TPDD::BuExitClick(TObject *Sender)
{
	 Close();
}
//---------------------------------------------------------------------------
void __fastcall TPDD::BuMenuClick(TObject *Sender)
{
	 tc->ActiveTab = Menu;
	 CountWrong=0;
	 CountCorrect=0;
	 laCor->Text="";
}
//---------------------------------------------------------------------------
void __fastcall TPDD::NextClick(TObject *Sender)
{
     tc->ActiveTab = Theory2;
}
//---------------------------------------------------------------------------
void __fastcall TPDD::Back2Click(TObject *Sender)
{
     tc->ActiveTab = Theory1;
}
//---------------------------------------------------------------------------
void __fastcall TPDD::Button1Click(TObject *Sender)
{
	 TheoryPage1->Scale->X -=0.1;
	 TheoryPage1->Scale->Y -=0.1;
	 TheoryPage1->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall TPDD::Button2Click(TObject *Sender)
{
	 TheoryPage1->Scale->X +=0.1;
	 TheoryPage1->Scale->Y +=0.1;
	 TheoryPage1->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall TPDD::FormCreate(TObject *Sender)
{
	tc->ActiveTab = Menu;
	CountCorrect=0;
    CountWrong=0;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image2Click(TObject *Sender)
{
	CountCorrect=CountCorrect + 1;
	tc->ActiveTab=TestPage2;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image3Click(TObject *Sender)
{
	CountWrong=CountWrong + 1;
	tc->ActiveTab=TestPage2;
}
//---------------------------------------------------------------------------



void __fastcall TPDD::Image4Click(TObject *Sender)
{
    CountWrong=CountWrong + 1;
	tc->ActiveTab=TestPage3;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image5Click(TObject *Sender)
{
	CountCorrect=CountCorrect + 1;
	tc->ActiveTab=TestPage3;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image7Click(TObject *Sender)
{
	CountCorrect=CountCorrect + 1;
	tc->ActiveTab=TestPage4;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image6Click(TObject *Sender)
{
	CountWrong=CountWrong + 1;
    if(CountWrong>=3)
	{
		ShowMessage("��� ������� �������� ������...");
		tc->ActiveTab=Menu;
		CountWrong=0;
		CountCorrect=0;
		laCor->Text="";
	}
	else
	{
	tc->ActiveTab=TestPage4;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image8Click(TObject *Sender)
{
	CountCorrect=CountCorrect + 1;
	tc->ActiveTab=Test;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image9Click(TObject *Sender)
{
	CountWrong=CountWrong + 1;
    if(CountWrong>=3)
	{
		ShowMessage("��� ������� �������� ������...");
		tc->ActiveTab=Menu;
		CountWrong=0;
		CountCorrect=0;
		laCor->Text="";
	}
	else
	{
	tc->ActiveTab=Test;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image11Click(TObject *Sender)
{
	CountCorrect=CountCorrect + 1;
	tc->ActiveTab=TestPage6;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image10Click(TObject *Sender)
{
	CountWrong=CountWrong + 1;
    if(CountWrong>=3)
	{
		ShowMessage("��� ������� �������� ������...");
        tc->ActiveTab=Menu;
		CountWrong=0;
		CountCorrect=0;
		laCor->Text="";
	}
	else
	{
	tc->ActiveTab=TestPage6;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image12Click(TObject *Sender)
{
	CountCorrect=CountCorrect + 1;
	laCor->Text="��� ���������: " + IntToStr(CountCorrect) + "/6";
	tc->ActiveTab=Result;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Image13Click(TObject *Sender)
{
	laCor->Text="��� ���������: " + IntToStr(CountCorrect) + "/6";
	CountWrong=CountWrong + 1;
    if(CountWrong>=3)
	{
		ShowMessage("��� ������� �������� ������...");
		tc->ActiveTab=Menu;
		CountWrong=0;
		CountCorrect=0;
		laCor->Text="";
	}
	else
	{
	tc->ActiveTab=Result;
    }
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Next2Click(TObject *Sender)
{
	tc->ActiveTab=Theory3;
}
//---------------------------------------------------------------------------

void __fastcall TPDD::Button14Click(TObject *Sender)
{
	tc->ActiveTab=Theory4;
}
//---------------------------------------------------------------------------

