//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Colors.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Layouts.hpp>
//---------------------------------------------------------------------------
class TPDD : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *Menu;
	TTabItem *Theory1;
	TTabItem *About;
	TColorButton *GameName;
	TLabel *NameOfGame;
	TImage *MenuImage;
	TColorButton *BuTest;
	TLabel *Label1;
	TColorButton *BuExit;
	TLabel *Label2;
	TColorButton *BuTheory;
	TLabel *Label3;
	TColorButton *BuAbout;
	TTabItem *TestPage1;
	TLabel *Label4;
	TToolBar *TBPage1;
	TButton *Back;
	TButton *Next;
	TButton *BuMenu;
	TLabel *Label5;
	TTabItem *Theory2;
	TToolBar *TBPage2;
	TButton *Back2;
	TButton *Next2;
	TButton *BuMenu2;
	TLabel *Label6;
	TImage *TheoryPage1;
	TImage *Image1;
	TToolBar *TBDPage1;
	TButton *Button1;
	TButton *Button2;
	TMemo *MemoAbout;
	TToolBar *ToolBar2;
	TButton *Button3;
	TLabel *Label7;
	TToolBar *TBDPage2;
	TButton *Button4;
	TButton *Button5;
	TToolBar *ToolBar1;
	TButton *Button8;
	TLabel *Label8;
	TTabItem *TestPage2;
	TTabItem *TestPage3;
	TTabItem *TestPage4;
	TTabItem *TestPage6;
	TToolBar *ToolBar3;
	TButton *Button6;
	TLabel *Label9;
	TToolBar *ToolBar4;
	TButton *Button7;
	TLabel *Label10;
	TToolBar *ToolBar5;
	TButton *Button9;
	TLabel *Label11;
	TToolBar *ToolBar6;
	TButton *Button10;
	TLabel *Label12;
	TToolBar *ToolBar7;
	TButton *Button11;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *Label15;
	TTabItem *Result;
	TToolBar *ToolBar8;
	TButton *Button12;
	TLabel *Label16;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image2;
	TImage *Image3;
	TLabel *Label17;
	TLabel *Label18;
	TLabel *Label19;
	TLabel *Label20;
	TLabel *Label21;
	TLabel *Label22;
	TLabel *Label23;
	TLabel *Label24;
	TLabel *Label25;
	TLabel *Label26;
	TTabItem *Test;
	TGridPanelLayout *GridPanelLayout2;
	TImage *Image4;
	TImage *Image5;
	TGridPanelLayout *GridPanelLayout3;
	TImage *Image6;
	TImage *Image7;
	TGridPanelLayout *GridPanelLayout4;
	TImage *Image8;
	TImage *Image9;
	TGridPanelLayout *GridPanelLayout5;
	TImage *Image10;
	TImage *Image11;
	TGridPanelLayout *GridPanelLayout6;
	TImage *Image12;
	TImage *Image13;
	TLabel *laCor;
	TLabel *laWr;
	TTabItem *Theory3;
	TToolBar *ToolBar9;
	TButton *Button13;
	TButton *Button14;
	TButton *Button15;
	TLabel *Label27;
	TImage *Image14;
	TToolBar *ToolBar10;
	TButton *Button16;
	TButton *Button17;
	TTabItem *Theory4;
	TToolBar *ToolBar11;
	TButton *Button18;
	TButton *Button19;
	TButton *Button20;
	TLabel *Label28;
	TImage *Image15;
	TToolBar *ToolBar12;
	TButton *Button21;
	TButton *Button22;
	TLayout *Layout2;
	TGridPanelLayout *GridPanelLayout7;
	TGridPanelLayout *GridPanelLayout8;
	void __fastcall BuTheoryClick(TObject *Sender);
	void __fastcall BuTestClick(TObject *Sender);
	void __fastcall BuAboutClick(TObject *Sender);
	void __fastcall BuExitClick(TObject *Sender);
	void __fastcall BuMenuClick(TObject *Sender);
	void __fastcall NextClick(TObject *Sender);
	void __fastcall Back2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Image2Click(TObject *Sender);
	void __fastcall Image3Click(TObject *Sender);

	void __fastcall Image4Click(TObject *Sender);
	void __fastcall Image5Click(TObject *Sender);
	void __fastcall Image7Click(TObject *Sender);
	void __fastcall Image6Click(TObject *Sender);
	void __fastcall Image8Click(TObject *Sender);
	void __fastcall Image9Click(TObject *Sender);
	void __fastcall Image11Click(TObject *Sender);
	void __fastcall Image10Click(TObject *Sender);
	void __fastcall Image12Click(TObject *Sender);
	void __fastcall Image13Click(TObject *Sender);
	void __fastcall Next2Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
private:
int CountCorrect;
int CountWrong;	// User declarations
public:		// User declarations
	__fastcall TPDD(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TPDD *PDD;
//---------------------------------------------------------------------------
#endif
