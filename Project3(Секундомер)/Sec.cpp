//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Sec.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFM *FM;
//---------------------------------------------------------------------------
__fastcall TFM::TFM(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFM::BuStartClick(TObject *Sender)
{
	FTimeStart = Now();
    Timer1->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFM::StopClick(TObject *Sender)
{
    Timer1->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TFM::Timer1Timer(TObject *Sender)
{
    LaTime->Text = TimeToStr(Now() - FTimeStart);
}
//---------------------------------------------------------------------------
