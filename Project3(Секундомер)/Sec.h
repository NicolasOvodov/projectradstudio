//---------------------------------------------------------------------------

#ifndef SecH
#define SecH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFM : public TForm
{
__published:	// IDE-managed Components
	TButton *BuStart;
	TButton *Stop;
	TLabel *LaTime;
	TTimer *Timer1;
	void __fastcall BuStartClick(TObject *Sender);
	void __fastcall StopClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
private:	// User declarations
TDateTime FTimeStart;
public:		// User declarations
	__fastcall TFM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFM *FM;
//---------------------------------------------------------------------------
#endif
