//---------------------------------------------------------------------------

#ifndef MemorizerH
#define MemorizerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.Math.hpp>
//---------------------------------------------------------------------------
class TNumberMemorizer : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buReset;
	TButton *buinf;
	TLabel *laCorrent;
	TLayout *LyO;
	TRectangle *Rectangle1;
	TRectangle *rectangle;
	TLabel *laWrong;
	TRectangle *reRemember;
	TLabel *laNumber;
	TLabel *laRemember;
	TProgressBar *pbRemember;
	TLabel *laAnswer;
	TEdit *edAnswer;
	TButton *buAnswer;
	TTimer *tiRemember;
	TButton *buZoomOut;
	TButton *buZoomIn;
	void __fastcall tiRememberTimer(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall buAnswerClick(TObject *Sender);
	void __fastcall edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall buZoomOutClick(TObject *Sender);
	void __fastcall buZoomInClick(TObject *Sender);
	void __fastcall buinfClick(TObject *Sender);
private:	// User declarations

int FCountWrong;
int FCountCorrent;
int FSecretNumber;

void doReset();
void doContinue();
void doAnswer();
void doViewQuestion(bool aValue);

public:		// User declarations
	__fastcall TNumberMemorizer(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TNumberMemorizer *NumberMemorizer;
//---------------------------------------------------------------------------
#endif
