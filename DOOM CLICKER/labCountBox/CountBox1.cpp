//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "CountBox1.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FListBox = new TList;
	for (int i = 1; i <= cMaxBox; i++){
		FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));

	}
	FListAnswer = new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);

	tc->ActiveTab=tiMain;



}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
//---------------------------------------------------------------------------
 void Tfm::DoReset()
 {
	 FCountCorrect = 0;
	 FCountWrong = 0;
	 FTimeValue = Time().Val + (float)30/(24*60*60);
	 tmPlay->Enabled = true;
	 DoContinue();

 }
 void Tfm::DoContinue()
 {
	 String filenamePNG=System::Ioutils::TPath::Combine("..\\..\\ImageRes", "Lava.png");
	 for (int i = 0; i < cMaxBox; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Bitmap->Bitmap->LoadFromFile(filenamePNG);
	 }

	 FNumberCorrect = RandomRange(cMinPossible,cMaxPossible);
	 int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	 String filenameJPG=System::Ioutils::TPath::Combine("..\\..\\ImageRes", "Soul.png");
	 for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Bitmap->Bitmap->LoadFromFile(filenameJPG); //= TAlphaColorRec::Crimson;

	 }


	 int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	 if (xAnswerStart < cMinPossible)
	 xAnswerStart = cMinPossible;

	 for (int i = 0; i < cMaxAnswer; i++) {
		 ((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart + i );
	 }


 }
 void Tfm::DoAnswer(int aValue)
 {
	 (aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	 if (FCountWrong > 5) {

	 DoFinish();
	 }
	 DoContinue();
 }
  void Tfm::DoFinish()
  {
	  String filenamePNG=System::Ioutils::TPath::Combine("..\\..\\ImageRes", "FW.png");
	  ImResult->Bitmap->LoadFromFile(filenamePNG);
	  tmPlay->Enabled = false;
	  tc->ActiveTab=tiFinish;
      if (FCountWrong > 5) {
	 Label1->Text=("�� ���������");
	 String filenamePNG=System::Ioutils::TPath::Combine("..\\..\\ImageRes", "FL.png");
	 ImResult->Bitmap->LoadFromFile(filenamePNG);
	 }

	  lacorrect->Text=Format(L"���������� = %d",
	 ARRAYOFCONST((FCountCorrect)));
      lawrong->Text=Format(L"������������ = %d",
	 ARRAYOFCONST((FCountWrong)));

  }
void __fastcall Tfm::buAnswer1Click(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if (x <= 0){
		DoFinish();
	}
}
//---------------------------------------------------------------------------


void __fastcall Tfm::bustartClick(TObject *Sender)
{
	tc->ActiveTab=tiPlay;
    DoReset();
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button3Click(TObject *Sender)
{
    tc->ActiveTab=tiInfo;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
    tc->ActiveTab=tiMain;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender)
{
    DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tiExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------



void __fastcall Tfm::buMainClick(TObject *Sender)
{
    tc->ActiveTab=tiMain;
}
//---------------------------------------------------------------------------




//---------------------------------------------------------------------------

