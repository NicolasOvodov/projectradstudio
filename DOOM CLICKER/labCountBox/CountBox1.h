//---------------------------------------------------------------------------

#ifndef CountBox1H
#define CountBox1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <System.IOUtils.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiPlay;
	TLayout *lyTop;
	TLayout *Layout1;
	TTimer *tmPlay;
	TLabel *laTime;
	TLabel *Label2;
	TButton *Button1;
	TGridPanelLayout *GridPanelLayout1;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TTabItem *tiInfo;
	TTabItem *tiFinish;
	TLayout *Layout2;
	TButton *Button2;
	TLayout *Layout4;
	TLabel *Label1;
	TLabel *lacorrect;
	TLabel *lawrong;
	TButton *buMain;
	TButton *burepeat;
	TImage *Image1;
	TGridPanelLayout *GridPanelLayout3;
	TImage *Image2;
	TButton *bustart;
	TButton *Button4;
	TButton *buexit;
	TImage *ImResult;
	TLayout *Layout5;
	TImage *Image3;
	TLabel *Label3;
	TMemo *Memo1;
	TImage *Image4;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buAnswer1Click(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall bustartClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall tiExitClick(TObject *Sender);
	void __fastcall buMainClick(TObject *Sender);
private:
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimeValue;
	TList *FListBox;
	TList *FListAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
	const int cMaxBox = 25;
	const int cMaxAnswer = 6;
	const int cMinPossible = 4;
	const int cMaxPossible = 14;


//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
