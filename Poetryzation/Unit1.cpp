//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}

inline int Low(const System::UnicodeString &)
{
#ifdef _DELPHI_STRING_ONE_BASED
return 1;
#else
return 0;
#endif
}
//
inline int High(const System::UnicodeString &S)
{
#ifdef _DELPHI_STRING_ONE_BASED
return S.Length();
#else
return S.Length() - 1;
#endif
}

//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
 System::UnicodeString x;
//
 for (int i=0; i<mfu->Lines->Count; i++)
 {
 x=mfu->Lines->Strings[i];
 if(i%2 == 1)
  {
  for (int j = Low(x); j <= High(x); j++)
   {
   if (x[j] != ' ')
   x[j] = '...';
   }
  }
 me1->Lines->Add(x);
 }
//
 bool xFlag;
 for (int i=0; i<mfu->Lines->Count; i++)
 { x = mfu->Lines->Strings[i];
 xFlag = false;
 for (int j = Low(x); j<= High(x); j++)
  {
  if((xFlag) && (x[j] !=' '))
  x[j] = '...';
  if((!xFlag) && (x[j] == ' '))
  xFlag = true;
  }
 me2->Lines->Add(x);
 }

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	ShowMessage("Made by Ovodov 151-362");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ZoomOutBtClick(TObject *Sender)
{
mfu->TextSettings->Font->Size -= 1;
me1->TextSettings->Font->Size -= 1;
me2->TextSettings->Font->Size -= 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ZoomInBtClick(TObject *Sender)
{
mfu->TextSettings->Font->Size += 1;
me1->TextSettings->Font->Size += 1;
me2->TextSettings->Font->Size += 1;
}
//---------------------------------------------------------------------------

