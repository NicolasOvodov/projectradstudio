//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "miniWeb.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buGoClick(TObject *Sender)
{
	wb -> URL = edURL -> Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	wb -> GoBack();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
    wb -> GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
    wb -> Stop();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    wb -> Reload();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	ShowMessage("LabWebBrowser - Zaharkina, Ovodov");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::wbDidFinishLoad(TObject *ASender)
{
	edURL -> Text = wb -> URL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key = vkReturn) {
		wb -> URL = edURL -> Text;
	}
}
//---------------------------------------------------------------------------
