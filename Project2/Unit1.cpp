//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TLogLoger *LogLoger;
//---------------------------------------------------------------------------
__fastcall TLogLoger::TLogLoger(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TLogLoger::AllClick(TObject *Sender)
{
  Memo1->Lines->Add(((TControl *) Sender) -> Name + ".OnClick");
}
//---------------------------------------------------------------------------
void __fastcall TLogLoger::ClearClick(TObject *Sender)
{
    Memo1->Lines->Clear();
}
//---------------------------------------------------------------------------
