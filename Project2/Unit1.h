//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TLogLoger : public TForm
{
__published:	// IDE-managed Components
	TMemo *Memo1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TCheckBox *CheckBox1;
	TCheckBox *CheckBox2;
	TButton *Clear;
	TPanel *Panel1;
	void __fastcall AllClick(TObject *Sender);
	void __fastcall ClearClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TLogLoger(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLogLoger *LogLoger;
//---------------------------------------------------------------------------
#endif
