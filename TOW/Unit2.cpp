//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TMenu *Menu;
//---------------------------------------------------------------------------
__fastcall TMenu::TMenu(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TMenu::BStartClick(TObject *Sender)
{
	Fm->Show();
}
//---------------------------------------------------------------------------


void __fastcall TMenu::BExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TMenu::BTMClick(TObject *Sender)
{
	TC1->ActiveTab = Main;
}
//---------------------------------------------------------------------------

void __fastcall TMenu::BRulesClick(TObject *Sender)
{
	TC1->ActiveTab = Rules;
}
//---------------------------------------------------------------------------

void __fastcall TMenu::BCreatorsClick(TObject *Sender)
{
    TC1->ActiveTab = Creators;
}
//---------------------------------------------------------------------------

