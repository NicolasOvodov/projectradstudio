//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFm *Fm;
//---------------------------------------------------------------------------
__fastcall TFm::TFm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TFm::FormCreate(TObject *Sender)
{
	x=0;
	IGO=false;
}
//---------------------------------------------------------------------------

void __fastcall TFm::PlayerOnePadClick(TObject *Sender)
{
	Score->Text = IntToStr(x);
	if (!IGO&&x>-110)
	{
		x-=10;
	}
	if (x<=-110)
	{
		IGO=true;
		ShowMessage ("������� ����� �����.");
		x = 0;
		Close();
		IGO=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFm::PlayerTwoPadClick(TObject *Sender)
{
	Score->Text = IntToStr(x);
	if (!IGO&&x<110)
	{
		x+=10;
	}
	if (x>=110)
	{
		IGO=true;
		ShowMessage ("������� ������� �����.");
        x = 0;
		Close();
		IGO=false;
	}
}
//---------------------------------------------------------------------------

