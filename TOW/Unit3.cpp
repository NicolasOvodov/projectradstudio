//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit3.h"
#include "Unit2.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TW1G *W1G;
//---------------------------------------------------------------------------
__fastcall TW1G::TW1G(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TW1G::Button1Click(TObject *Sender)
{
	Menu->Show();
	Close();
}
//---------------------------------------------------------------------------
