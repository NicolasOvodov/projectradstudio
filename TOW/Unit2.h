//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TMenu : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TC1;
	TTabItem *Main;
	TTabItem *Rules;
	TLabel *GN;
	TGridPanelLayout *GridPanelLayout1;
	TButton *BStart;
	TButton *BRules;
	TButton *BCreators;
	TButton *BExit;
	TTabItem *Creators;
	TImage *Image1;
	TToolBar *ToolBar1;
	TButton *BTM;
	TLabel *Label1;
	TMemo *MR;
	TToolBar *ToolBar2;
	TButton *BTM2;
	TLabel *Label2;
	TMemo *Memo1;
	void __fastcall BStartClick(TObject *Sender);
	void __fastcall BExitClick(TObject *Sender);
	void __fastcall BTMClick(TObject *Sender);
	void __fastcall BRulesClick(TObject *Sender);
	void __fastcall BCreatorsClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TMenu(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMenu *Menu;
//---------------------------------------------------------------------------
#endif
