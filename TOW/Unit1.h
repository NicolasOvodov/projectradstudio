//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Score;
	TRectangle *PlayerOnePad;
	TRectangle *PlayerTwoPad;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall PlayerOnePadClick(TObject *Sender);
	void __fastcall PlayerTwoPadClick(TObject *Sender);
private:	// User declarations
    int x;
	bool IGO;
public:		// User declarations
	__fastcall TFm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm *Fm;
//---------------------------------------------------------------------------
#endif
